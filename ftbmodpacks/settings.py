# -*- coding: utf-8 -*-

# Scrapy settings for ftbmodpacks project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#
BOT_NAME = 'ftbmodpacks'
SPIDER_MODULES = ['ftbmodpacks.spiders']
NEWSPIDER_MODULE = 'ftbmodpacks.spiders'
ITEM_PIPELINES = {
    'ftbmodpacks.pipelines.FtbmodpacksSrvValidPipeline': 1,
    'ftbmodpacks.pipelines.FtbmodpacksSrvExportPipeline': 10,
}

# CONCURRENT_ITEMS = 100
# CONCURRENT_REQUESTS = 16
# DEPTH_PRIORITY = 0
# REACTOR_THREADPOOL_MAXSIZE = 10
# COOKIES_ENABLED = False
DEPTH_LIMIT = 4
# CLOSESPIDER_TIMEOUT = 60

EXPORT_PATH = "/tmp/ftbmodpacks"
EXPORT_FORMATS = ['JSON', 'CSV', 'XML']
EXPORT_NAME = 'ftbmodpacks'
FEED_EXPORT_FIELDS = [
    "modpack_name",
    "modpack_author",
    "zip_server_version",
    "zip_client_version",
    "zip_date",
    "zip_server_name",
    "zip_server_url",
    "zip_server_size",
    "zip_server_mcversion",
    "zip_client_name",
    "zip_client_url",
    "zip_client_size",
    "zip_client_mcversion",
    "url",
    "modpack_icon",
]

LOG_LEVEL = 'INFO'
LOG_FORMAT = '%(asctime)s [%(name)s] %(levelname)s: %(message)s'
LOG_DATEFORMAT = '%Y-%m-%d %H:%M:%S'
LOG_FILE = 'ftbmodpacks.log'

# FEED_STORE_EMPTY = False
# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'ftbmodpacks (+http://www.yourdomain.com)'

# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class FtbItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    url = scrapy.Field()
    modpack_name = scrapy.Field()
    modpack_author = scrapy.Field()
    modpack_icon = scrapy.Field()
    zip_date = scrapy.Field()
    zip_client_mcversion = scrapy.Field()
    zip_client_version = scrapy.Field()
    zip_client_name = scrapy.Field()
    zip_client_url = scrapy.Field()
    zip_client_size = scrapy.Field()
    zip_server_mcversion = scrapy.Field()
    zip_server_version = scrapy.Field()
    zip_server_name = scrapy.Field()
    zip_server_url = scrapy.Field()
    zip_server_size = scrapy.Field()

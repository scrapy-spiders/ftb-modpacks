# -*- coding: utf-8 -*-
import scrapy
import logging
import time
import locale
import re
from ftbmodpacks.items import FtbItem
from scrapy.linkextractors import LinkExtractor
from scrapy.spiders import CrawlSpider, Rule


class FtbSpider(CrawlSpider):
    name = 'ftb'
    allowed_domains = ['www.feed-the-beast.com']
    ftb_url = 'http://www.feed-the-beast.com'
    start_urls = [ftb_url + '/modpacks']
    valid_url = list()
    rejected_url = list()
    valid_urls = 0
    rejected_urls = 0

    @staticmethod
    def build_xpaths():
        lst = dict()
        lst['modpack_name'] = \
            '//h1[@class="project-title"]/a/span/text()'
        lst['modpack_author'] = \
            '//div[@class="user-tag"]/a/text()'
        lst['modpack_icon'] = \
            '//div[@class="avatar-wrapper"]/a/img/@src'
        lst['zip_date'] = \
            '//td[@class="project-file-date-uploaded"]/abbr/@data-epoch'
        lst['zip_client_mcversion'] = \
            '//section[@class="details-versions"]/ul/li/text()'
        lst['zip_client_name'] = \
            '//div[@class="details-info"]/ul/li[1]/' \
            'div[@class="info-data overflow-tip"]/text()'
        lst['zip_client_url'] = \
            '//div[@class="project-file-download-button-large"]/a/@href'
        lst['zip_client_size'] = \
            '//div[@class="details-info"]/ul/li[3]/' \
            'div[@class="info-data"]/text()'
        lst['zip_server_mcversion'] = \
            '//tr[@class="project-file-list-item"]/td[5]/span/text()'
        lst['zip_server_name'] = \
            '//tr[@class="project-file-list-item"]/td[2]/div/' \
            'div[@class="project-file-name-container"]/a/text()'
        lst['zip_server_url'] = \
            '//tr[@class="project-file-list-item"]/td[2]/div/' \
            'div[@class="project-file-download-button"]/a/@href'
        lst['zip_server_size'] = \
            '//tr[@class="project-file-list-item"]/' \
            'td[@class="project-file-size"]/text()'
        return lst

    @staticmethod
    def build_pcrawl():
        lst = dict()
        p_pages = ur'.*/modpacks\?page=[0-9]+$'
        lst['pages'] = re.compile(p_pages, re.IGNORECASE)
        p_modpack = ur'.*/projects/[^/]+$'
        lst['modpack'] = re.compile(p_modpack, re.IGNORECASE)
        p_modpackFiles = ur'.*/projects/[^/]+/files(\?page=[0-9]+)?$'
        lst['modpackFiles'] = re.compile(p_modpackFiles, re.IGNORECASE)
        p_archive = ur'.*/projects/[^/]+/files/[0-9]+$'
        lst['archive'] = re.compile(p_archive, re.IGNORECASE)
        return lst

    @staticmethod
    def build_pscrape():
        lst = dict()
        lst['client_filename'] = re.compile(
            ur'^.*-([0-9\.]+)-[0-9\.]+.zip$', re.IGNORECASE)
        lst['server_filename'] = re.compile(
            ur'^.*-([0-9\.]+)-[0-9\.]+[-|_]?(server)?.zip$', re.IGNORECASE)
        return lst

    @classmethod
    def extract(cls, response, elt):
        path = cls.x_paths[elt]
        return response.xpath(path).extract()

    # item fields xpaths
    x_paths = build_xpaths.__func__()
    # Crawl patterns
    pcr = build_pcrawl.__func__()
    # Scrape patterns
    psc = build_pscrape.__func__()

    rules = (
        Rule(
            LinkExtractor(
                allow=(
                    pcr['pages'],
                    pcr['modpack'],
                    pcr['modpackFiles'],))),
        Rule(
            LinkExtractor(
                allow=(
                    pcr['archive'],
                )), callback='parse_item'),
    )

    def parse_item(self, response):
        url = re.sub('\?cookieTest=.*$', '', response.url)
        check_path = '//h4[@id="additional-files"]/text()'
        if len(response.xpath(check_path).extract()) > 0:
            i = FtbItem()
            i['url'] = url
            # Set global attributes
            i['modpack_name'] = self.extract(
                response, 'modpack_name')[0].strip()
            i['modpack_author'] = self.extract(
                response, 'modpack_author')[0].strip()
            i['modpack_icon'] = self.extract(
                response, 'modpack_icon')[0].strip()
            # dateFormat = "%Y/%m/%d"
            dateFormat = "%s"
            rawDate = float(self.extract(response, 'zip_date')[0].strip())
            formattedDate = time.strftime(
                dateFormat,
                time.gmtime(rawDate))
            # formattedDate.decode(locale.getdefaultlocale()[1])
            i['zip_date'] = formattedDate
            # Set client archive attributes
            i['zip_client_mcversion'] = self.extract(
                response, 'zip_client_mcversion')[0].strip()
            i['zip_client_name'] = self.extract(
                response, 'zip_client_name')[0].strip()
            i['zip_client_url'] = self.ftb_url + self.extract(
                response, 'zip_client_url')[0].strip()
            i['zip_client_size'] = self.extract(
                response, 'zip_client_size')[0].strip()
            v = re.search(self.psc['client_filename'], i['zip_client_name'])
            if v:
                i['zip_client_version'] = v.group(1)
            else:
                i['zip_client_version'] = 'N/A'
            # Set server archive attributes
            i['zip_server_mcversion'] = self.extract(
                response, 'zip_server_mcversion')[0].strip()
            i['zip_server_name'] = self.extract(
                response, 'zip_server_name')[0].strip()
            i['zip_server_url'] = self.ftb_url + self.extract(
                response, 'zip_server_url')[0].strip()
            i['zip_server_size'] = self.extract(
                response, 'zip_server_size')[0].strip()
            v = re.search(self.psc['server_filename'], i['zip_server_name'])
            if v:
                i['zip_server_version'] = v.group(1)
            else:
                i['zip_server_version'] = i['zip_client_version']
            # self.valid_url += url
            self.valid_urls += 1
            self.logger.debug('Server Zip found on %s' % url)
            return i
        else:
            # self.rejected_url += url
            self.rejected_urls += 1
            self.logger.debug('NO Server Zip found on %s' % url)

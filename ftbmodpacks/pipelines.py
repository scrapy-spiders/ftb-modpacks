# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html
import logging
import os
from scrapy.conf import settings
from scrapy.exporters import JsonItemExporter, XmlItemExporter, CsvItemExporter


class FtbmodpacksSrvValidPipeline(object):
    logger = logging.getLogger('validation')

    def open_spider(self, spider):
        self.valid_items = 0
        self.rejected_items = 0

    def close_spider(self, spider):
        # URLS Validation Status
        total_urls = spider.valid_urls + spider.rejected_urls
        self.logger.info(
            'Valid urls : %i/%i',
            spider.valid_urls,
            total_urls)
        self.logger.info(
            'Rejected urls : %i/%i',
            spider.rejected_urls,
            total_urls)
        if spider.rejected_urls == total_urls:
            self.logger.error('All FTB modpack file pages have been rejected!')
        # Items Validation Status
        total_items = self.rejected_items + self.valid_items
        self.logger.info(
            'Valid items : %i/%i',
            self.valid_items,
            total_items)
        self.logger.info(
            'Rejected items : %i/%i',
            self.rejected_items,
            total_items)
        if self.rejected_items == total_items:
            self.logger.error('All items have been rejected!')

    def process_item(self, item, spider):
        valid = True
        if item['modpack_name'] is None or len(item['modpack_name']) < 1:
            self.logger.warning('modpack_name missing for %s !', item.url)
            valid = False
        if item['modpack_author'] is None or len(item['modpack_author']) < 1:
            self.logger.warning('modpack_author missing for %s !', item.url)
            valid = False
        if item['modpack_icon'] is None or len(item['modpack_icon']) < 1:
            self.logger.warning('modpack_icon missing for %s !', item.url)
            pass
        if item['zip_date'] is None or len(item['zip_date']) < 1:
            self.logger.warning('zip_date missing for %s !', item.url)
            valid = False
        if item['zip_client_mcversion'] is None or len(item['zip_client_mcversion']) < 1:
            self.logger.warning('zip_client_mcversion missing for %s !', item.url)
            valid = False
        if item['zip_client_version'] is None or len(item['zip_client_version']) < 1:
            self.logger.warning('zip_client_version missing for %s !', item.url)
            valid = False
        if item['zip_client_name'] is None or len(item['zip_client_name']) < 1:
            self.logger.warning('zip_client_name missing for %s !', item.url)
            valid = False
        if item['zip_client_url'] is None or len(item['zip_client_url']) < 1:
            self.logger.warning('zip_client_url missing for %s !', item.url)
            valid = False
        if item['zip_client_size'] is None or len(item['zip_client_size']) < 1:
            self.logger.warning('zip_client_size missing for %s !', item.url)
            valid = False
        if item['zip_server_mcversion'] is None or len(item['zip_server_mcversion']) < 1:
            self.logger.warning('zip_server_mcversion missing for %s !', item.url)
            valid = False
        if item['zip_server_version'] is None or len(item['zip_server_version']) < 1:
            self.logger.warning('zip_server_version missing for %s !', item.url)
            valid = False
        if item['zip_server_name'] is None or len(item['zip_server_name']) < 1:
            self.logger.warning('zip_server_name missing for %s !', item.url)
            valid = False
        if item['zip_server_url'] is None or len(item['zip_server_url']) < 1:
            self.logger.warning('zip_server_url missing for %s !', item.url)
            valid = False
        if item['zip_server_size'] is None or len(item['zip_server_size']) < 1:
            self.logger.warning('zip_server_size missing for %s !', item.url)
            valid = False
        if item['zip_client_version'] != item['zip_server_version']:
            valid = False
        if valid:
            self.valid_items += 1
            return item
        else:
            self.rejected_items += 1


class FtbmodpacksSrvExportPipeline(object):
    logger = logging.getLogger('export')
    # exporters = list()
    # files = list()

    def __init__(self):
        self.itemCount = 0
        self.files = dict()
        self.exporters = dict()
        self.exportpath = settings.get('EXPORT_PATH', '/tmp')
        self.exportname = settings.get('EXPORT_NAME', 'ftbmodpacks')
        self.exportfields = settings.get('FEED_EXPORT_FIELDS', None)
        self.exportformats = settings.get('EXPORT_FORMATS', [])
        if not os.path.isdir(self.exportpath):
            os.mkdir(self.exportpath)
        self.logger.info("exportpath: %s" % self.exportpath)
        self.logger.info("exportname: %s" % self.exportname)
        self.logger.info("exportfields: %s" % self.exportfields)
        self.logger.info("exportformats: %s" % self.exportformats)

    def open_spider(self, spider):
        self.files[spider] = list()
        self.exporters[spider] = list()
        self.startExporters(spider)

    def startExporters(self, spider):
        if 'JSON' in self.exportformats:
            filename = os.path.join(self.exportpath, "{}.json".format(self.exportname))
            f = open(filename, 'w+b')
            exporter = JsonItemExporter(f, fields_to_export=self.exportfields)
            exporter.start_exporting()
            self.files[spider].append(f)
            self.exporters[spider].append(exporter)
            self.logger.info('Exporting to %s', filename)
        if 'CSV' in self.exportformats:
            filename = os.path.join(self.exportpath, "{}.csv".format(self.exportname))
            f = open(filename, 'w+b')
            exporter = CsvItemExporter(f, True, ', ', fields_to_export=self.exportfields)
            exporter.start_exporting()
            self.files[spider].append(f)
            self.exporters[spider].append(exporter)
            self.logger.info('Exporting to %s', filename)
        if 'XML' in self.exportformats:
            filename = os.path.join(self.exportpath, "{}.xml".format(self.exportname))
            f = open(filename, 'w+b')
            exporter = XmlItemExporter(f, item_element='modpack', root_element='ftb-modpacks',
                                       fields_to_export=self.exportfields)
            exporter.start_exporting()
            self.files[spider].append(f)
            self.exporters[spider].append(exporter)
            self.logger.info('Exporting to %s', filename)

    def close_spider(self, spider):
        for exporter in self.exporters.pop(spider):
            exporter.finish_exporting()
        self.logger.info(
            '%i items, representing feed-the-beast modpacks server archives,'
            ' have been written to %s!',
            self.itemCount,
            self.exportpath)
        for file in self.files.pop(spider):
            file.close()

    def process_item(self, item, spider):
        self.itemCount += 1
        for exporter in self.exporters[spider]:
            exporter.export_item(item)
        return item

# scrapy-spiders/ftb-modpacks

This scrapy spider (http crawler) will extract ftb modpacks server data in json, csv or xml

## Installation

1. Make sure you've installed [scrapy](http://scrapy.org/)
2. Clone this repository:
  `git clone https://framagit.org/scrapy-spiders/ftb-modpacks`
3. cd ftb-modpacks


## Usage

Here's a short explanation how to use `ftb-modpacks`:

* launch a custom crawl with :
    scrapy crawl ftb -s LOG_FILE=<logfile> -s EXPORT_PATH=<export(s)_path> -s EXPORT_FORMATS="['JSON','XML','CSV']"


## Contributing

1. Fork it
2. Create your feature branch: `git checkout -b feature/my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin feature/my-new-feature`
5. Submit a pull request

## Requirements / Dependencies

* [scrapy](http://scrapy.org/)

## Version

0.1.0

## License

Copyright (C) <year> <name of author>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
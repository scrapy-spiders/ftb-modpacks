# Automatically created by: shub deploy

from setuptools import setup, find_packages

setup(
    name='project',
    version='1.0',
    description='Crawl feed-the-beast',
    long_description='open("README.md").read()',
    author='gp3t1',
    author_email='gp3t1@openmailbox.org',
    url='https://framagit.org/scrapy-spiders/ftb-modpacks.git',
    packages=find_packages(exclude=['*tests*']),
    entry_points={'scrapy': ['settings = ftbmodpacks.settings']},
)
